import styled from 'styled-components/native';
//import { Header, Title, SubTitle } from './styles';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

export const BackGroundApp = styled.View `
    background-color: #FAFAFA;
    width: ${wp('100%')};
    height: ${hp('100%')};
    padding: 15px;
    align-items: center;
    justify-content: center;
`
export const ViewForm = styled.View `
    height: ${hp('50%')};
    justify-content: flex-start;
    margin-top: 10%;
`
export const ViewForm2 = styled.View `
    width: ${wp('60%')};
    height: ${hp('50%')};
    align-items: center;
    justify-content: flex-end;
    margin-bottom: 10%;

`

export const LogoApp = styled.Image `
    width: 90%;
    height: 50%;
    resize-mode: contain;
`

export const TextForm = styled.Text `
    font-weight: 600;
    color: #000000;
    font-size: 16px;
    margin-left: 15px;
    font-family: OpenSans;
    text-align: left;
`

export const InputForm = styled.TextInput `
    width: ${wp('90%')};
    font-family: OpenSans;
    font-weight: Regular;
    color: #000000;
    font-size: 14px;
    background: #FFFFFF;
    padding: 10px;
    text-align: left;
    border: 1px solid rgba(0, 0, 0, .4);
    border-radius: 5px;
    margin: 5px 15px 28px 15px;

`

export const Button = styled.TouchableOpacity `
    width: ${wp('90%')};
    padding: 4%;
    
    background: #0092CA;
    border-radius: 5px;
    margin-left: 15px;


`
export const TextButton = styled.Text`
    text-align: center;
    font-weight: 400;
    font-size: 16px;
    font-family: OpenSans;

    
    color: #EEEEEE;

`
export const GoToLogin = styled.Text `
    text-align: center;
    font-size: 14px;
    font-weight: 400;
    margin-top: 30px;
    font-family: OpenSans;
`