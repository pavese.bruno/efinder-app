import styled from 'styled-components/native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';


export const BackGroundApp = styled.View `
    background-color: #FFFFFF;
    width: ${wp('100%')};
    height: ${hp('100%')};
`

export const ImageBoard = styled.Image `
    width: 70%;
    height: 70%;
    resize-mode: contain;

`
export const ImageDots = styled.Image `
    width: 10%;
    height: 10%;
    resize-mode: contain;
    margin-bottom: 20px;

`
export const TextBoard1 = styled.Text `
    font-weight: 800;
    color: #000000;
    font-size: 20px;
    font-family: OpenSans;
`
export const TextBoard2 = styled.Text `
    font-weight: 400;
    color: #000000;
    font-size: 14px;
    font-family: OpenSans;
    text-align: center;
    margin-top: 30px;
`
export const ViewBoard = styled.View `
    height: ${hp('55%')};   
    resize-mode: contain;
    align-items: center;
    justify-content: flex-end;

`
export const ViewBoard2 = styled.View `
    height: ${hp('45%')};   
    resize-mode: contain;
    align-items: center;
    justify-content: flex-start;
    margin-top: 51px;

`
export const ViewBoard3 = styled.View `
    height: ${hp('20%')};   
    resize-mode: contain;
    align-items: center;
    justify-content: flex-start;

`
export const Button = styled.TouchableOpacity `
    width: ${wp('90%')};
    padding: 4%;
    background: #0092CA;
    border-radius: 5px;


`
export const TextButton = styled.Text`
    text-align: center;
    font-weight: 400;
    font-size: 16px;
    font-family: OpenSans;
    
    color: #EEEEEE;

`