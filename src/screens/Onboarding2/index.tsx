import React from 'react';
//import { Image, View, Text, TouchableOpacity } from 'react-native';
import { useForm } from 'react-hook-form';
import { useNavigation } from '@react-navigation/native';
import { ImageBoard, BackGroundApp, ViewBoard, ViewBoard2, ViewBoard3, Button, TextButton,
         TextBoard1, TextBoard2, ImageDots } from './styles';


export default function Onboarding2() {

    const navigation = useNavigation();

    const { control } = useForm({mode:'onTouched'});
    
    return (
        <BackGroundApp>
            <ViewBoard>
            <ImageBoard source={require('../../../assets/Onboarding2.png')}></ImageBoard>
            </ViewBoard>
            <ViewBoard2>
                <ViewBoard3>
                    <TextBoard1>Venda Conosco</TextBoard1>
                    <TextBoard2>Comece seu negócio aqui<br/>ou expanda o seu e deixe mais<br/>fácil de organizar</TextBoard2>
                </ViewBoard3>
                <ImageDots source={require('../../../assets/dots2.png')}></ImageDots>
                <Button onPress={() => navigation.navigate("Cadastro")}>
                    <TextButton>Próximo</TextButton>
                </Button>
            </ViewBoard2>



        </BackGroundApp>



    )


}