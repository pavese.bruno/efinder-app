import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import { useFonts } from 'expo-font';

import Home from './src/screens/Home';
import Login from './src/screens/Login';
import Cadastro from './src/screens/Cadastro';
import Onboarding1 from './src/screens/Onboarding1';
import Onboarding2 from './src/screens/Onboarding2';

const Stack = createStackNavigator();

export default function App() {

  const [ loaded ] = useFonts({
    OpenSans: require('./assets/assetsFonts/OpenSans-VariableFont_wdth,wght.ttf'),
    Poetsen: require('./assets/assetsFonts/PoetsenOne-Regular.ttf'),
  })

  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Onboarding1" component={Onboarding1} options={{
          headerShown: false
        }}/>
        <Stack.Screen name="Onboarding2" component={Onboarding2} options={{
          headerShown: false
        }}/>
        <Stack.Screen name="Cadastro" component={Cadastro} options={{
          headerShown: false
        }}/>
        <Stack.Screen name="Login" component={Login} options={{
          headerShown: false
        }}/>
      </Stack.Navigator>
    </NavigationContainer>
  );
}